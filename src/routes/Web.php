<?php


namespace Devspark\lrs\routes;

use Devspark\lrs\controllers\UserController;
use http\Url;

include_once('src/controllers/UserController.php');


class Web
{
    private static $instance;
    protected static $routes;

    private function __construct()
    {
        self::$instance = null;
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Web();
        }
        return self::$instance;
    }

    public function addRoute(
        string $name,
        array $methods,
        string $url,
        array $params,
        $controllerClass,
        $action

    )
    {
        $pattern = str_replace('/', '\/', $url);
        $pattern = '/' . '(' . implode('|', $methods) . ')' . $pattern . '$/';
        foreach ($params as $key => $value) {
            $pattern = str_replace($key, $value . '+', $pattern);
        }
        self::$routes[$name] = [
            'pattern' => $pattern,
            'class' => $controllerClass,
            'list' => $action,
            'params' => $params
        ];

    }

    public function run($url)
    {
        foreach (self::$routes as $key => $route) {
            if (preg_match($route['pattern'], $_SERVER['REQUEST_METHOD'] . $url)) {
                if ($route['params']!= null) {
                    $parameter = self::getData($route['pattern'], $url);
                }
                $list = $route['list'];
                $class = new $route['class'];
                return $class->$list($parameter);
            }

        }
    }

    private function getData($pattern, $url)
    {
        preg_match('/(\[){1}[0-9-]+(\]){1}/', $pattern, $match);
        preg_match("/$match[0]+/", $url, $data);
        return $data['0'];
    }
}