<?php


namespace Devspark\lrs\system;


class View
{
    private static $sections = [];
    private static $layout;

    public static function sections($key, $value)
    {
        self::$sections[$key] = $value;
    }

    public static function yieldC($key)
    {
        echo self::$sections[$key];
    }

    public static function extend($path){
        $path = 'src/views/layouts/'.$path.'.phtml';
        if (!file_exists($path)){

            throw new \Exception('layout file does not exist');
        }
        self::$layout = $path;

    }

    public static function render($path, $data = [])
    {
        $path = "src/views/pages/".$path.".phtml";
        if(!file_exists($path)){
            throw new \Exception('View file not exist');
        }
        extract($data);
        ob_start();
        require_once $path;
        $output = ob_get_clean();

        if (self::$layout !== null){
            ob_start();
            require_once self::$layout;
            $output = ob_get_clean();
        }
        return $output;
    }

    public static function startSection($name){

        ob_start();
        self::$sections[$name] = null;
    }
    public static function endSection($name){

        self::$sections[$name] = ob_get_clean();
    }



}