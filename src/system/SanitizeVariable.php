<?php


namespace Devspark\lrs\system;


class SanitizeVariable
{
    public static function sanitize($variable, $default, $method)
    {
    $type = gettype($default);

    if($method === 'post'){
        $sanVariable = htmlspecialchars($_POST[$variable]);

       if (!isset($sanVariable)){
          $sanVariable = $default;
       }
       return $sanVariable;
    }
        if($method === 'get'){
             $sanVariable = htmlspecialchars($_GET[$variable]);
            if (!isset($sanVariable)){
                $sanVariable = $default;
            }
            return $sanVariable;
        }
    }
}