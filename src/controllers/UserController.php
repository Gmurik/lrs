<?php


namespace Devspark\lrs\controllers;


use Devspark\lrs\system\View;

class UserController
{
public  function listUser()
{
    return View::render('home',['title' => 'Home page']);
}
public function createForm(){
    return View::render('createUser',['title' => 'Add user']);
}

}