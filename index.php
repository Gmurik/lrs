<?php

use Devspark\lrs\controllers\UserController;
use Devspark\lrs\routes\Web;

include_once 'autoload.php';
$route = Web::getInstance();
$route->addRoute('userList',['GET'],'/user',[],UserController::class,'listUser');
$route->addRoute('createUserGet',['GET'],'/user/create',[],UserController::class,'createForm');
echo $route->run($_SERVER['REQUEST_URI']);
